# tkdcm

Extensions to tkinter widgets for displaying and working with DICOM images.  Uses pillow (PIL fork), tkinter, numpy and pydicom.